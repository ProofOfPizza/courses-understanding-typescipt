import { RequestHandler } from 'express';
import { Todo } from '../models/todo';

let TODOS: Array<Todo> = [];

export const createTodo: RequestHandler = (req, res, __) => {
  const text = (req.body as { text: string }).text;
  const newTodo = new Todo(Math.random().toString(), text);
  TODOS.push(newTodo);
  res.status(201).json({ message: 'Created todo:', createdTodo: newTodo.id });
};

export const getTodos: RequestHandler = (_, res, __) => {
  return res.status(200).json({ todos: TODOS });
};

export const patchTodo: RequestHandler = (req, res, _) => {
  const patchRequest = req.body as { id: string; newText: string };
  const { id, newText } = patchRequest;
  TODOS.find((todo) => {
    if (todo.id === id) {
      todo.text = newText;
    }
  });
  res.status(201).json({ updatedTodo: id });
};

export const deleteTodo: RequestHandler<{ id: string }> = (req, res, _) => {
  const id = req.params.id;
  TODOS = TODOS.filter((todo) => todo.id !== id);
  res.status(201).json({ deletedTodo: id });
};

