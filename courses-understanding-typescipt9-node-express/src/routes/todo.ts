import { Router } from 'express';
import { createTodo, getTodos, patchTodo, deleteTodo } from '../controllers/todo';
const router = Router();

router.post('/', createTodo);
router.get('/', getTodos);
router.patch('/', patchTodo);
router.delete('/:id', deleteTodo);

export default router;

