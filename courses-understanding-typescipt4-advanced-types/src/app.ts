// intersection type
type Admin = {
  name: string;
  priviledges: string[];
};

type Employee = {
  name: string;
  startDate: Date;
};

type elevatedEmployee = Admin & Employee;

const e1: elevatedEmployee = {
  name: "Chai",
  priviledges: ["learn ts"],
  startDate: new Date(),
};
//const e2: elevatedEmployee = {
//  name: "jim",
//  startDate: new Date(),
//};
// fails: priviledges are missing

// can also be done with interfaces and extends
// for objects is is the combination Of couse our e1 ca be both a Employee and an Admin !
//
type Inputs = string | boolean;
type Outputs = number | boolean;

type inOut = Inputs & Outputs;

const inOuts: inOut = true; // only boolean, but watch out

// typeguards:
const add = (a: Inputs, b: Inputs) => {
  //next line is an example of a typeguard
  if (typeof a === "boolean" || typeof b === "boolean") {
    return a && b;
  } else {
    return a + b;
  }
};
// another example:

type unknownEmployee = Admin | Employee;
const printEmployeeInfo = (emp: unknownEmployee) => {
  console.log(emp.name);
  //console.log(emp.priviledges) not allowed.. maybe not exists Also typeof is no help because at runtime all are 'object' . We now use ... in
  if ("priviledges" in emp) {
    console.log(emp.priviledges);
  }
  if ("startDate" in emp) {
    console.log(emp.startDate);
  }
};
// another is instanceof
class Car {
  drive() {
    console.log("driving");
  }
}
class Truck {
  drive() {
    console.log("driving trucks");
  }
  loadCargo(stuff: string) {
    console.log("loading", stuff);
  }
}
type Vehicle = Car | Truck;
const useVehicle = (vehicle: Vehicle) => {
  if (vehicle instanceof Truck) {
    vehicle.loadCargo("food");
  }
  if (vehicle instanceof Car) {
    vehicle.drive();
  }
};
//
// discriminated union as typeguard:
// useful for onject types, classes or interfaces

interface CanFly {
  type: "bird";
  flyingSpeed: number;
}
interface CanWalk {
  type: "horse";
  groundSpeed: number;
}
type Animal = CanWalk | CanFly;

const moveAnimal = (animal: Animal) => {
  let speed;
  switch (animal.type) {
    case "bird":
      speed = animal.flyingSpeed;
      break;
    case "horse":
      speed = animal.groundSpeed;
      break;
  }
  console.log(speed);
};

// type Casting
//see index.html
const pararaph = document.getElementById("message-output");
const userInput = document.getElementById("user-input");
//userInput.value = "hello"; // error: might be null and any sort of elemement

const userInput1 = <HTMLInputElement>document.getElementById("user-input")!;
userInput1.value = "hello"; // ok: ! and casted.

const userInput2 = document.getElementById("user-input")! as HTMLInputElement;
userInput1.value = "hello"; // ok: ! and casted.

//or, maybe better cuz no !:
if (userInput) {
  (userInput as HTMLInputElement).value = "blip";
}

// index types

interface ErrorContainer {
  // should be flexible error container
  // we do not know up front wich elements the objects container
  [prop: string]: string; // is the index type and all other fiels must now be same type:
  //  id: string;
}

const errorBag: ErrorContainer = {
  email: "invalid email",
  username: "wrong name",
};

// function overloads:

function addIt(a: boolean, b: boolean): boolean;
function addIt(a: string, b: string): string;
function addIt(a: string, b: boolean): string;
function addIt(a: Inputs, b: Inputs): Inputs {
  //next line is an example of a typeguard
  if (typeof a === "boolean" && typeof b === "boolean") {
    return a && b;
  } else {
    return a.toString() + b.toString();
  }
}

let str = addIt("okay", false);
str.split(""); // no problem, type is clear.

// for anonymous see:
// https://stackoverflow.com/questions/39187614/typescript-overload-arrow-functions

// Optional chaining.
// useful if it is uncertain if a object contains certain properties
const fetchedUserData = {
  id: "u1",
  name: "jim",
  job: { title: "CEO", description: "my own company" },
};

console.log(fetchedUserData.job);
// but what to do if we are unsure if stuff exists?
// say api stuff... to avoid js runtime err:
if (fetchedUserData && fetchedUserData.job) {
  //something
}

//in ts:
console.log(fetchedUserData?.job?.title);
//
// nullish coalescing. Make sure something is reall null | undefined and not jst 'falsy'

console.log("" || "DEFAULT");
console.log("" ?? "DEFAULT");

