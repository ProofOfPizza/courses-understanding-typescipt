import axios from 'axios';
//declare var google: any;

const form = document.querySelector('form')!;
const addressInput = document.getElementById('address')! as HTMLInputElement;
const GOOGLE_API_KEY = 'AIzaSyCKAwHr6HIyK6O8EIq7FhCvPptAB4HYZzA';

type GoogleGeocodingResponse = {
  results: Array<{ geometry: { location: { lat: number; lng: number } } }>;
  status: 'OK' | 'ZERO_RESULTS';
};

const searchAddress = (event: Event) => {
  event.preventDefault();
  const address = addressInput.value;

  const mapsUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURI(
    address,
  )}&key=${GOOGLE_API_KEY}`;
  axios
    .get<GoogleGeocodingResponse>(mapsUrl)
    .then((response) => {
      if (response.data.status === 'ZERO_RESULTS') {
        throw new Error('No results found');
      }

      const location = response.data.results[0].geometry.location;
      const mapElement = document.getElementById('map') as HTMLElement;
      if (mapElement) {
        const map = new google.maps.Map(mapElement, {
          center: location,
          zoom: 13,
        });
        new google.maps.Marker({
          position: location,
          map: map,
        });
      } else {
        throw new Error('no element found to render map');
      }
    })
    .catch((error) => {
      alert(error);
      console.log(error);
    });
};

form.addEventListener('submit', searchAddress);

