{ pkgs ? import (fetchTarball https://github.com/NixOS/nixpkgs/archive/7ed3d2df76cd1f637c6163858ac0e1db9cdf7b00.tar.gz) {} }:
pkgs.mkShell {
  buildInputs = with pkgs; [
    nodejs-14_x
    yarn
    nodePackages.typescript
    # keep this line if you use bash
    #pkgs.bashInteractive
  ];
}
