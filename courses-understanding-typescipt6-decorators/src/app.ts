// Decorators
// be sure to: enable ES6 and experimentalDecorators in tsconfig!
// decorators are function with at leas 1 arg

// runs at instantiating the class... not when creating objects

function Logger(constructor: Function) {
  console.log('logging');
  console.log(constructor);
}

@Logger
class Person {
  name = 'max';

  constructor() {
    console.log('creating person');
  }
}

const p = new Person();

// decorator factory
function LoggerFact(logstring: string) {
  return (constructor: Function) => {
    console.log(logstring);
    console.log(constructor);
  };
}

function WithTemplate(template: string, hookId: string) {
  return (_: Function) => {
    const element = document.getElementById(hookId);
    if (element) {
      element.innerHTML = template;
    }
  };
}

@WithTemplate('<h1>My Person Obj</h1>', 'app')
@LoggerFact('log this line for person')
class PersonP {
  name = 'max';

  constructor() {
    console.log('creating person');
  }
}
//this way we can give a value to the decorator that is returned when we execute the () decorator.
// multiple decoratorfactories top tons o bottom, but the inner decorator functrun from bottom to top

// decorators in other places:

function Log(target: any, propertyName: string | Symbol) {
  console.log('Log');
  console.log(target, propertyName);
}
function Log2(target: any, name: string | Symbol, propertyDescriptor: PropertyDescriptor) {
  console.log('accessor decorator Log');
  console.log(target);
  console.log(name);
  console.log(propertyDescriptor);
}
function Log3(target: any, name: string | Symbol, propertyDescriptor: PropertyDescriptor) {
  console.log('method decorator Log');
  console.log(target);
  console.log(name);
  console.log(propertyDescriptor);
}

function Log4(target: any, name: string | Symbol, position: number) {
  console.log('parameter decorator Log');
  console.log(target);
  console.log(name);
  console.log(position);
}

class Product {
  @Log // executes when class is registered
  title: string;
  private _price: number;

  @Log2
  set price(@Log4 val: number) {
    if (val > 0) {
      this._price = val;
    }
  }
  get price() {
    return this._price;
  }

  @Log3
  displayProduct() {
    console.log(this.title, this.price);
  }
  constructor(t: string, p: number) {
    this.title = t;
    this._price = p;
  }
}
// we can also return values. in a class decorator... a new cnstructor / class :
//

function WithTemplate2(template: string, hookId: string) {
  // extends new is anything new-able, and so we can accept instantiating a class with any number of any arguments ...args: any[]
  return <T extends { new (...args: any[]): { name: string } }>(oldConstructor: T) => {
    return class extends oldConstructor {
      args: any[];
      constructor(...args: any[]) {
        super();
        const element = document.getElementById(hookId);
        if (element) {
          element.innerHTML = template;
        }
        this.args = args;
      }
      printArgs() {
        console.log('instantiated with ', this.args);
      }
    };
  };
}
// this now returns a new class (syntactic sugar for costrunctor function).
// The newly created constructor will of coure only run when instantiating an object.
@WithTemplate2('<h1>bla</h1>', 'app')
class Person2 {
  name = 'max';

  constructor(..._: any[]) {
    console.log('creating person');
  }
  printArgs() {}
}

const p2 = new Person2('toet', 10, { bell: 'ring' });
p2.printArgs();

// return values for decorators are usable for: methods, accessors, classes. On properties and parameters it is ignored.
// you can return a new descriptor.
// the autobind
//

function Autobind(_: any, __: string, descriptor: PropertyDescriptor): PropertyDescriptor {
  console.log('Autobind Log');
  const originalMethod = descriptor.value;
  const newMethod: PropertyDescriptor = {
    configurable: true,
    enumerable: false,
    get() {
      const boundFn = originalMethod.bind(this);
      return boundFn;
    },
  };
  // no value, just a getter. This here refers to the obect calling the method which is the obj we want.
  return newMethod;
}

class Printer {
  message = 'this works';
  @Autobind
  showMessage() {
    console.log(this.message);
  }
}
const printer = new Printer();
const button = document.querySelector('button')!;
//button.addEventListener('click', printer.showMessage.bind(printer));

// bind because this -> refers to target of eventlistener, not printer
// another elegant way to fix it with decorator:
button.addEventListener('click', printer.showMessage.bind(printer));

// validations as decorators

interface validatorConfig {
  [property: string]: {
    // the class name to validte
    [validatableProp: string]: string[]; // ['required', 'positiveNumber']
  };
}

const registeredValidators: validatorConfig = {};
function Required(target: any, propName: string) {
  registeredValidators[target.constructor.name] = {
    ...registeredValidators[target.constructor.name],
    [propName]: [...registeredValidators[target.constructor.name][propName], 'required'],
  };
}

function PositiveNumber(target: any, propName: string) {
  registeredValidators[target.constructor.name] = {
    ...registeredValidators[target.constructor.name],
    [propName]: [...registeredValidators[target.constructor.name][propName], 'positive'],
  };
}
function validate(obj: any): boolean {
  const objValidatorConfig = registeredValidators[obj.constructor.name];
  if (!objValidatorConfig) {
    console.log('no obj to validate');
    return true;
  }
  for (const prop in objValidatorConfig) {
    for (const validator in objValidatorConfig[prop]) {
      console.log('validator', validator);
      switch (validator) {
        case 'Required':
          !!obj[prop];
        case 'PositiveNumber':
          return obj[prop] > 0;
      }
    }
  }
  console.log('skipped it all', objValidatorConfig);
  return true;
}

class Course {
  @Required
  title: string;
  @PositiveNumber
  price: number;

  constructor(t: string, p: number) {
    this.price = p;
    this.title = t;
  }
}
const courseForm = document.querySelector('form')!;
courseForm.addEventListener('submit', (event) => {
  event.preventDefault();
  const titleEl = document.getElementById('title') as HTMLInputElement;
  const priceEl = document.getElementById('price') as HTMLInputElement;

  const title = titleEl.value;
  const price = +priceEl.value;
  const createdCourse = new Course(title, price);
  if (validate(createdCourse)) {
    console.log('created:', createdCourse);
  } else {
    alert('NEIN');
  }
});
console.log(registeredValidators);

