enum Food {
  PIZZA = "pizza",
  COOKIES = "cookies",
  SOUP = "song",
}

enum Bla {
  BLIP,
  BLUU,
}

const person: {
  name: string;
  age: number;
  hobbies: string[];
  role: [number, string];
  food: Food;
  whatever: Bla;
} = {
  name: "Chai",
  age: 30,
  hobbies: ["spsassdsdoarts", "cook"],
  role: [2, "author"],
  food: Food.PIZZA,
  whatever: Bla.BLIP,
};

person.role = [2, "oke"];
console.log(person);

