type Combinable = number | string;
type ConversionType = "as-number" | "as-text";

function combina(
  input1: Combinable,
  input2: Combinable,
  returnType: ConversionType
) {
  let result;
  if (typeof input1 === "number" && typeof input2 === "number") {
    result = input1 + input2;
  } else {
    result = input1.toString() + input2.toString();
  }
  if (returnType === "as-text") {
    return result.toString();
  } else {
    return +result;
  }
}

console.log(combina("harry", 44, "as-text"));
console.log(combina("22", 44, "as-number"));
console.log(combina(22, 44, "as-text"));

