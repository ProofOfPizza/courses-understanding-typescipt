function add(n1: number, n2: number) {
  return n1 + n2;
}

function printResult(num: number) {
  console.log(add(num, 17));
}

printResult(4);

function addAndHandle(n1: number, n2: number, cb: (num: number) => void) {
  const result = add(n1, n2);
  cb(result);
}

addAndHandle(12, 44, (res) => {
  console.log("gezellig", res);
});

let combine: Function;
combine = add;
combine = console.log;

let combi: (a: number, b: number) => number;
combi = add;
//combi = toString();

