let userInput: unknown;
let userName: string;

userInput = 5;
userInput = "max";
//userName = userInput;

if (typeof userInput === "string") {
  userName = userInput;
}

const generateError = (message: string, code: number): never => {
  throw { message: message, errorCode: code };
};

generateError("problems!", 555);

// never does not even return undefined!

