import { ComponentBaseClass } from './base';
import { Project } from './project';
import { Autobind } from '../decorators/autobind';
import { Draggable } from '../models/drag-drop';

export class ProjectItem extends ComponentBaseClass<HTMLUListElement, HTMLLIElement> implements Draggable {
  private project: Project;

  get persons() {
    return this.project.people === 1 ? '1 person registered.' : `${this.project.people} persons registered.`;
  }
  constructor(hostElementId: string, project: Project) {
    super('single-project', hostElementId, 'beforeend', project.id);
    this.project = project;
    this.configure();
    this.renderContent();
  }
  @Autobind
  dragStartHandler(event: DragEvent): void {
    console.log(event);
    event.dataTransfer!.setData('text/plain', this.project.id);
    event.dataTransfer!.effectAllowed = 'move';
  }

  dragEndHandler(_: DragEvent): void {
    console.log('drag ending...');
  }
  configure() {
    this.element.addEventListener('dragstart', this.dragStartHandler);
    this.element.addEventListener('dragend', this.dragEndHandler);
  }
  renderContent() {
    this.element.querySelector('h2')!.textContent = this.project.title;
    this.element.querySelector('h3')!.textContent = this.persons;
    this.element.querySelector('p')!.textContent = this.project.description;
  }
}

