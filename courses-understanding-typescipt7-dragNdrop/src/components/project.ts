import { ProjectStatus } from './project-status';

export class Project {
  private _id: string;
  private _status: ProjectStatus;
  private _title: string;
  private _description: string;
  private _people: number;

  constructor(t: string, d: string, p: number) {
    this._id = Math.random().toString();
    this.title = t;
    this.description = d;
    this.people = p;
    this.status = ProjectStatus.ACTIVE;
  }
  public get id(): string {
    return this._id;
  }
  public set status(s: ProjectStatus) {
    this._status = s;
  }
  public get status() {
    return this._status;
  }
  public set title(t: string) {
    this._title = t;
  }
  public get title() {
    return this._title;
  }
  public set description(d: string) {
    this._description = d;
  }
  public get description() {
    return this._description;
  }
  public set people(p: number) {
    this._people = p;
  }
  public get people() {
    return this._people;
  }
}

