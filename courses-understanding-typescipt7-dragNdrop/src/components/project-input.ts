import { ComponentBaseClass } from './base';
import { Project } from './project';
import { validateProjectInput } from '../util/validate';
import { Autobind } from '../decorators/autobind';
import { State } from '../state/state';

export class ProjectInput extends ComponentBaseClass<HTMLDivElement, HTMLFormElement> {
  titleInputElement: HTMLInputElement;
  descriptionInputElement: HTMLInputElement;
  peopleInputElement: HTMLInputElement;

  constructor() {
    super('project-input', 'app', 'afterbegin', 'user-input');
    this.titleInputElement = this.element.querySelector('#title') as HTMLInputElement;
    this.descriptionInputElement = this.element.querySelector('#description') as HTMLInputElement;
    this.peopleInputElement = this.element.querySelector('#people') as HTMLInputElement;
    this.configure();
  }

  renderContent() {}
  private gatherUserInput(): Project | void {
    const { isValid, errors } = validateProjectInput(
      this.titleInputElement.value,
      this.descriptionInputElement.value,
      +this.peopleInputElement.value,
    );
    if (isValid) {
      const project = new Project(
        this.titleInputElement.value,
        this.descriptionInputElement.value,
        +this.peopleInputElement.value,
      );
      this.clearInputs();
      return project;
    } else {
      alert(errors);
      return;
    }
  }

  private clearInputs() {
    this.titleInputElement.value = '';
    this.descriptionInputElement.value = '';
    this.peopleInputElement.value = '';
  }

  @Autobind
  private submitHandler() {
    event?.preventDefault();
    const project = this.gatherUserInput();
    if (project) {
      State.getInstance().addProject(project);
      console.log(State.getInstance());
    }
  }

  configure() {
    this.element.addEventListener('submit', this.submitHandler);
  }
}

