import { ComponentBaseClass } from './base';
import { Project } from './project';
import { ProjectStatus } from './project-status';
import { ProjectItem } from './project-item';
import { State } from '../state/state';
import { Autobind } from '../decorators/autobind';
import { DragTarget } from '../models/drag-drop';

export class ProjectsList extends ComponentBaseClass<HTMLDivElement, HTMLElement> implements DragTarget {
  assignedProjects: Array<Project>;

  constructor(private type: ProjectStatus) {
    super('project-list', 'app', 'beforeend', `${type}-projects`);
    this.configure();
    this.renderContent();
  }
  @Autobind
  dragOverHandler(event: DragEvent): void {
    if (event.dataTransfer && event.dataTransfer.types[0] === 'text/plain') {
      const listElement = this.element.querySelector('ul')!;
      listElement.classList.add('droppable');
      event.preventDefault();
    }
  }
  @Autobind
  dropHandler(event: DragEvent): void {
    console.log('drop');
    const projectId = event.dataTransfer!.getData('text/plain');
    const targetStatus = this.type;
    console.log('...', projectId, targetStatus);
    State.getInstance().moveProject(projectId, targetStatus);
  }
  @Autobind
  dragLeaveHandler(_: DragEvent): void {
    const listElement = this.element.querySelector('ul')!;
    listElement.classList.remove('droppable');
  }

  configure() {
    this.element.addEventListener('dragover', this.dragOverHandler);
    this.element.addEventListener('drop', this.dropHandler);
    this.element.addEventListener('dragleave', this.dragLeaveHandler);
    State.getInstance().addListener((projects: Array<Project>) => {
      this.assignedProjects = projects;
      this.renderPojects();
    });
  }
  private renderPojects() {
    const listElement = document.getElementById(`${this.type}-projects-list`)! as HTMLUListElement;
    listElement.innerHTML = '';
    this.assignedProjects = this.assignedProjects.filter((project) => project.status === this.type);
    for (const project of this.assignedProjects) {
      new ProjectItem(this.element.querySelector('ul')!.id, project);
    }
  }
  renderContent() {
    const listId = `${this.type}-projects-list`;
    this.element.querySelector('ul')!.id = listId;
    this.element.querySelector('h2')!.textContent = `${this.type.toUpperCase()} PROJECTS`;
  }
}

