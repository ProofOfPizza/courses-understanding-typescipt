import { ValidationResult } from './validation-result';
import { Validatable } from './validatable';

export const validate = (validatables: Validatable[]): ValidationResult => {
  let result: ValidationResult = { isValid: true, errors: [] };
  validatables.forEach((validatable) => {
    if (validatable.required && !validatable.value.toString()) {
      result.errors.push(`${validatable.field} is required`);
    }
    if (typeof validatable.value === 'string') {
      if (validatable.minLength && validatable.value.length < validatable.minLength) {
        result.errors.push(`${validatable.field} should be at least ${validatable.minLength} characters`);
      }
      if (validatable.maxLength && validatable.value.length > validatable.maxLength) {
        result.errors.push(`${validatable.field} should be max ${validatable.maxLength} characters`);
      }
    }

    if (typeof validatable.value === 'number') {
      if (validatable.min && validatable.value < validatable.min) {
        result.errors.push(`${validatable.field} should be at least ${validatable.min}`);
      }
      if (validatable.max && validatable.value > validatable.max) {
        result.errors.push(`${validatable.field} should be max ${validatable.max}`);
      }
    }
  });
  result.isValid = result.errors.length === 0;
  return result;
};

export const validateProjectInput = (title: string, description: string, people: number) => {
  const validateThis: Validatable[] = [];
  validateThis.push({
    field: 'title',
    value: title,
    minLength: 4,
    maxLength: 40,
  });
  validateThis.push({
    field: 'description',
    value: description,
    required: true,
    minLength: 5,
  });
  validateThis.push({
    field: 'people',
    value: people,
    required: true,
    min: 0,
    max: 80,
  });
  return validate(validateThis);
};

