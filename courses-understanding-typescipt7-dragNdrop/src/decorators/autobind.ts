export function Autobind(_: any, __: any, descriptor: PropertyDescriptor): PropertyDescriptor {
  const oldMethod = descriptor.value;
  const newMethod: PropertyDescriptor = {
    configurable: true,
    enumerable: false,
    get() {
      const boundFn = oldMethod.bind(this);
      return boundFn;
    },
  };
  return newMethod;
}

