import _ from 'lodash';
import { ProjectInput } from './components/project-input';
import { ProjectsList } from './components/projects-list';
import { ProjectStatus } from './components/project-status';

new ProjectInput();
new ProjectsList(ProjectStatus.ACTIVE);
new ProjectsList(ProjectStatus.FINISHED);
console.log('__ lodash', _.max([1, 2, 44, 5, 6]));

