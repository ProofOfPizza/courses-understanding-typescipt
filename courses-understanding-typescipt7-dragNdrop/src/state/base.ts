import { Listener } from './listener';

export abstract class StateBase<T> {
  protected listeners: Array<Listener<T>>;

  public addListener(listener: Listener<T>) {
    this.listeners.push(listener);
  }
}

