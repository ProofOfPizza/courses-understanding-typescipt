import { StateBase } from './base';
import { Project } from '../components/project';
import { ProjectStatus } from '../components/project-status';

export class State extends StateBase<Project> {
  private static instance: State;
  private projects: Array<Project>;

  private constructor() {
    super();
    this.projects = [];
    this.listeners = [];
  }

  public static getInstance() {
    if (!this.instance) {
      this.instance = new State();
    }
    return this.instance;
  }
  private updateListeners() {
    for (const listener of this.listeners) {
      listener(this.projects.slice()); //slice makes a copy, so nothing gets changed unnoticed and no other fn can change state.
    }
  }
  public addProject(project: Project) {
    this.projects.push(project);
    this.updateListeners();
  }
  public moveProject(projectId: string, targetStatus: ProjectStatus) {
    this.projects.map((p) => {
      if (p.id === projectId) {
        p.status = targetStatus;
      }
    });
    this.updateListeners();
  }
}

