// Generics

const names1 = ["jim", "joe"]; //type [] ..Array<T> ..any
const names2: Array<string> = ["jim", "joe"];
const names3: Array<string | boolean> = ["jim", "joe", true];

const promise: Promise<string> = new Promise((resolve) => {
  setTimeout(() => {
    resolve("long enough");
  }, 2000);
});

promise.then((data) => {
  data.split("");
});

// our own generics. First generic function

const merge = (objA: object, objB: object) => {
  Object.assign(objA, objB);
};

const merged = merge({ name: "jim" }, { age: 30 });
//merged.name; ERROR type 'void'

const mergeGeneric = <T, U>(objA: T, objB: U) => {
  Object.assign(objA, objB);
};

const mergedG = mergeGeneric({ name: "jim" }, { age: 30 });
console.log("31", mergedG);
//mergedG.name; // err

const mergedFail = mergeGeneric({ name: "jim" }, 30); // compiles but does not work!

//better with constrains if possible:

const mergeGeneric2 = <T extends object, U extends object>(
  objA: T,
  objB: U
) => {
  return Object.assign(objA, objB);
};

const mergedG2 = mergeGeneric2({ name: "jim" }, { age: 30 });
mergedG2.name; // is fine cuz type is T & U

//const mergedFail2 = mergeGeneric2({name: 'jim'} 30}); ERR !

// keyof constrain://
const extractAndConvert = <T extends object, U extends keyof T>(
  obj: T,
  key: U
): string => {
  return "value " + obj[key];
};

extractAndConvert({ name: "joey" }, "name");

// generic classes:
//
class DataStorage<T extends string | boolean | number> {
  private data: Array<T> = [];
  addItem(item: T) {
    this.data.push(item);
  }
  removeItem(item: T) {
    this.data.splice(this.data.indexOf(item), 1);
    // very unsafe: for objects, new obj has new reference... if ot found returns -1 and removes last element!
  }
  getItems() {
    return [...this.data];
  }
}

const textStorage = new DataStorage<string>();
type user = { name: string; email: string; age: number };
//const userStorage = new DataStorage<user>(); // gives problems!

// generic Utility types
interface CourseGoal {
  title: string;
  description: string;
  completeUntil: Date;
}

const createCourseGoal = (
  title: string,
  description: string,
  completeUntil: Date
): CourseGoal => {
  let courseGoal: Partial<CourseGoal> = {};
  courseGoal.title = title;
  courseGoal.description = description;
  courseGoal.completeUntil = completeUntil;
  return courseGoal as CourseGoal;
};

const names: Readonly<string[]> = ["Max", "Anna"];
//names.push("piet"); ERR!

// see https://www.typescriptlang.org/docs/handbook/utility-types.html

enum MaybeType {
  Just = "Just",
  Nothing = "Nothing",
}

interface Just<T> {
  type: typeof MaybeType.Just;
  value: T;
}

interface Nothing {
  type: typeof MaybeType.Nothing;
}

type Maybe<T> = Just<T> | Nothing;

const Nothing = (): Nothing => ({
  type: MaybeType.Nothing,
});

const Just = <T>(value: T): Just<T> => ({
  type: MaybeType.Just,
  value,
});

const Justi = <T>(value: T): Just<T> => {
  return {
    type: MaybeType.Just,
    value,
  };
};
const j1 = Just(1);
const j2 = Justi(2); // with () is just syntax apparently
console.log(j1);
console.log(j2);

const maybeOf = <T>(value: T): Maybe<T> => {
  return value === null || value === undefined ? Nothing() : Just(value);
};

console.log(maybeOf(console.log()));
console.log(maybeOf(4 + 2));

