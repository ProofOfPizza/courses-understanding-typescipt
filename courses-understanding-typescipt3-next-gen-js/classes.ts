// Code goes here
class Department {
  name: string;

  constructor(n: string) {
    this.name = n;
  }

  describe(): void {
    console.log("Department", this.name);
  }
  describeThis(this: Department): void {
    //this should be an instance of department!
    console.log("Department", this.name);
  }
}
const accounting = new Department("Acc");
console.log(accounting);
console.log(typeof accounting);

const accountObj = { describe: accounting.describe }; // refers  to the function it self, without executing
accountObj.describe(); // undefined! this here is accountObj, not accounting !

// for fix see describeThis
const accountObjSafe = { describeThis: accounting.describeThis };
//accountObjSafe.describeThis(); ERROR !
const accountObjSafeOk = {
  name: "some department",
  describe: () => {},
  describeThis: accounting.describeThis,
};

accountObjSafeOk.describeThis();

const accountObjSafeOk2: Department = {
  name: "some department",
  describe: () => {},
  describeThis: accounting.describeThis,
};

accountObjSafeOk2.describeThis();

type SomeOtherDepartmentType = Department;
const accountObjSafeOk3: SomeOtherDepartmentType = {
  name: "some department",
  describe: () => {},
  describeThis: accounting.describeThis,
};

accountObjSafeOk3.describeThis();

type SomeNonDepartmentType = {
  name: string;
  describe: () => void;
  describeThis: () => void;
};
const accountObjSafeNok: SomeNonDepartmentType = {
  name: "not some department",
  describe: () => {},
  describeThis: accounting.describeThis,
};

accountObjSafeNok.describeThis();

//shorthand for iniializing fields:
//
class School {
  constructor(
    public schoolName: string,
    private level: 0 | 1 | 2,
    private numberOfStudents: number
  ) {}
  public printMe(this: School) {
    console.log(this.numberOfStudents, this.level, this.schoolName, "duh");
  }
  set newNumberOfStudents(num: number) {
    this.numberOfStudents = num;
  }
  get currentNumberOfStudents(): number {
    return this.numberOfStudents;
  }
}

const college = new School("Alberda", 2, 250);
console.log(college);
college.printMe();

class SwimmingSchool extends School {
  constructor(
    schoolName: string,
    numberOfStudents: number,
    public lifeGuardName: string
  ) {
    super(schoolName, 2, numberOfStudents); // must be the first arg! level 2 is here set by default
  }
  public printsMe(this: SwimmingSchool) {
    console.log(this.schoolName, this.lifeGuardName);
  }
}

const swims = new SwimmingSchool("schwimm", 40, "jim");
console.log(swims);
swims.printsMe();
swims.printMe();
console.log(swims.currentNumberOfStudents);
swims.newNumberOfStudents = 22;
console.log(swims.currentNumberOfStudents);

class HeadQuarter extends School {
  private static instance: HeadQuarter;
  private constructor() {
    super("HQ", 0, 0);
  }
  static getInstance() {
    if (this.instance) return this.instance;
    // this here refers to the CLASS! maybe more clearly:
    // HeadQuarter.instance
    else {
      return new HeadQuarter();
    }
  }
}
const hq: HeadQuarter = HeadQuarter.getInstance();
const hq1: School = HeadQuarter.getInstance();
const aa: HeadQuarter = new School("schl", 2, 33);

