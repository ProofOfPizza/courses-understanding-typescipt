interface Named {
  readonly name: string;
}

interface Greetable extends Named {
  title?: string; //? optional
  greet(): void;
}

let user1: Greetable;

user1 = {
  name: "joe",
  greet() {
    console.log("hi my name is", this.name);
  },
};

class Person implements Greetable {
  constructor(public name: string) {}
  greet() {
    console.log(`hello you' re talikng to ${this.name}`);
  }
}
// interface as function type
type addFn = (a: number, b: number) => number;

//as interfac:
interface addFnInterface {
  (a: number, b: number): number;
}

let add: addFnInterface;
//add(n:number) => {return 1+ n}; ERROR
add = (a: number, b: number) => {
  return a + b;
};

