// Code goes here
const hobbies = ["sports", "cooking"];
const activeHobbies = ["hiking"];

// activeHobbies.push(hobbies); illegal
activeHobbies.push(hobbies[0], hobbies[1]);
//legal: we CAN push to const array!
//nicer:
//
activeHobbies.push(...hobbies);

const niceHobbies = ["starazing", ...hobbies];

//or objects
type Person = { username: string; age: number; gender: "m" | "v" | "o" };
const person: Person = { username: "jim", age: 14, gender: "m" };

const copiedPerson = person;
// copies the pointer to the person
// to create a real copy:
//
const reallyCopiedPerson = { ...person };
console.log({ person });
console.log({ copiedPerson });
console.log({ reallyCopiedPerson });

person.age = 15;
console.log({ person });
console.log({ copiedPerson });
console.log({ reallyCopiedPerson });

// rest parameters
const concater = (...strs: string[]) => {
  return strs.reduce((prev, next) => {
    return prev + next;
  });
};

const concaterR = (...strs: string[]) => {
  return strs.reduceRight((prev, next) => {
    return prev + next;
  });
};
console.log(concater("hi", "there", "jane", "baby!"));
console.log(concaterR("hi", "there", "jane", "baby!"));

//destructuring
const [hobby1, hobby2] = hobbies;
const [hobby, ...remainingHobby] = hobbies;

const { username, ...rest } = person;
const { username: voornaam, age } = person;
console.log({ rest });
console.log(voornaam);

const match = (p: Person): void => {
  const { username: name, age } = p;
  console.log(name, " has age", age.toString());
};

match(person);

